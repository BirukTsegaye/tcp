package com.example.bk.tcb;

/**
 * Created by bk on 3/23/2018.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.JsonElement;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.api.AIServiceException;
import ai.api.RequestExtras;
import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.model.AIContext;
import ai.api.model.AIError;
import ai.api.model.AIEvent;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import ai.api.model.Status;
import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;

public class activity_message_list extends AppCompatActivity {

    private AIDataService aiDataService;
    public static final String TAG = AITextSampleActivity.class.getName();

    String message;
    ChatView chatView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);

        //chat view ui and default response
        chatView = (ChatView) findViewById(R.id.chat_view);
        chatView.addMessage(new ChatMessage("እንኳን ወደ ኢትዮቴሌኮም በሰላም መጡ", System.currentTimeMillis(), ChatMessage.Type.RECEIVED));
        chatView.addMessage(new ChatMessage("ምን ልርዳዎት？", System.currentTimeMillis(), ChatMessage.Type.RECEIVED));
        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
            @Override
            public boolean sendMessage(ChatMessage chatMessage) {
                message = chatMessage.getMessage().toString();
               // Toast.makeText(activity_message_list.this, message, Toast.LENGTH_SHORT).show();
                sendRequest();
                return true;
            }
        });

        chatView.setTypingListener(new ChatView.TypingListener() {
            @Override
            public void userStartedTyping() {

            }

            @Override
            public void userStoppedTyping() {

            }
        });

        initService(new LanguageConfig("en", "a11ea1d839e3446d84e402cb97cdadfb"));


    }
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(AISettingsActivity.class);
            return true;
        }
        if(id == R.id.action_voice){
            startActivity(STT.class);
            return true;
        }
        if(id == R.id.action_about){
            startActivity(About.class);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startActivity(Class<?> cls) {
        final Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        final LanguageConfig selectedLanguage = (LanguageConfig) parent.getItemAtPosition(position);
//        selectedLanguage.getLanguageCode();
//
//    }



    private void initService(final LanguageConfig selectedLanguage) {
        final AIConfiguration.SupportedLanguages lang = AIConfiguration.SupportedLanguages.fromLanguageTag(selectedLanguage.getLanguageCode());
        final AIConfiguration config = new AIConfiguration(Config.ACCESS_TOKEN,
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);


        aiDataService = new AIDataService(this, config);
    }



    private void sendRequest() {

        final String queryString = message;
        //final String queryString = chatMessage.getMessage().toString()
       final String eventString = null;
        final String contextString = "";

        if (TextUtils.isEmpty(queryString) && TextUtils.isEmpty(eventString)) {
            onError(new AIError(getString(R.string.non_empty_query)));
            return;
        }

        final AsyncTask<String, Void, AIResponse> task = new AsyncTask<String, Void, AIResponse>() {

            private AIError aiError;

            @Override
            protected AIResponse doInBackground(final String... params) {
                final AIRequest request = new AIRequest();
                String query = params[0];
                String event = params[1];

                if (!TextUtils.isEmpty(query))
                    request.setQuery(query);
                if (!TextUtils.isEmpty(event))
                    request.setEvent(new AIEvent(event));
                final String contextString = params[2];
                RequestExtras requestExtras = null;
                if (!TextUtils.isEmpty(contextString)) {
                    final List<AIContext> contexts = Collections.singletonList(new AIContext(contextString));
                    requestExtras = new RequestExtras(contexts, null);
                }

                try {
                    return aiDataService.request(request, requestExtras);
                } catch (final AIServiceException e) {
                    aiError = new AIError(e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(final AIResponse response) {
                if (response != null) {
                    onResult(response);
                } else {
                    onError(aiError);
                }
            }
        };

        task.execute(queryString, null, "");
    }


    private void onResult(final AIResponse response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //status result
                final Status status = response.getStatus();

                //response
                final Result result = response.getResult();

                //finall speech
                final String speech = result.getFulfillment().getSpeech();

                //Toast.makeText(activity_message_list.this, speech, Toast.LENGTH_SHORT).show();
                chatView.addMessage(new ChatMessage(speech, System.currentTimeMillis(), ChatMessage.Type.RECEIVED));

//               if(speech.contains("994")){
//                   chatView.setOnClickListener(new View.OnClickListener() {
//                       @Override
//                       public void onClick(View view) {
//                           Toast.makeText(activity_message_list.this, "check", Toast.LENGTH_SHORT).show();
//                       }
//                   });
//               }

                final Metadata metadata = result.getMetadata();
                if (metadata != null) {

                }

                final HashMap<String, JsonElement> params = result.getParameters();
                if (params != null && !params.isEmpty()) {
                    Log.i(TAG, "Parameters: ");
                    for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
                        Log.i(TAG, String.format("%s: %s", entry.getKey(), entry.getValue().toString()));
                    }
                }
            }

        });
    }
    private void onError(final AIError error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity_message_list.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onBackPressed() {

            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(new ContextThemeWrapper(activity_message_list.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert));
            builder.setMessage("Are you sure you want to exit?")
                    .setTitle("Exit")
                    .setPositiveButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();

                        }
                    })

                    .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            System.exit(0);
                        }
                    }).create();
            builder.show();


        }



}